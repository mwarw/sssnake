OPTION DOTNAME
option casemap :none  ; case sensitive


include \masm32\include\gdi32.inc
include \masm32\include\user32.inc
include \masm32\include\kernel32.inc
includelib \MASM32\LIB\gdi32.lib
includelib \MASM32\LIB\user32.lib
includelib \MASM32\LIB\kernel32.lib

.DATA
	mapin QWord		0	;char[][]
	directions QWord 0	;Vector2Int[][] 
	output QWord 0		;char[][]
	position QWORD	0	;Vecotr2 int
	tileIn byte	0		;zmienna na obecny tile
	targetPosition QWORD 0	;zmienna pomocnicza na docelowe pole
	hInstance qword 0

.DATA?
emptyTile qword ?
appleTile qword ?
snakeTile qword ?
headTile qword ?

.code
;1 �mier�
;3 jab�ko
;0 nic

MapUpdate proc 
	mov mapin, RCX
	mov directions, RDX
	mov output, R9
	mov position, R8

	mov rax, position
	call VectorSplicer
	mov rax, mapin
	call ArrayAccByte

	mov tileIn, bl

	cmp tileIn, 0
	je end_noAction

	cmp tileIn, 2
	je moving

	cmp tileIn, 1
	je moving	
	
	;cmp tileIn, 3
	;je end_noAction

	end_noAction:
		mov rax, position
		call VectorSplicer
		mov rax, output
		call ArrayAccByte
		
		cmp bl, 1
		je just_ret
		cmp bl, 2
		je just_ret
		
		mov rax, output
		mov bl, tileIn
		call ArraySetByte

		just_ret: 
			mov rax, 0
			ret

	moving:
		;dost�p do kierunku we w�a�ciwym polu
		mov rax, position
		call VectorSplicer
		mov rax, directions
		call ArrayAccQword

		movq mm0, rbx
		mov rbx, position
		movq mm1, rbx
		paddd mm0, mm1
		movq rax, mm0
		mov targetPosition, rax

		cmp tileIn, 2
		je tail_move

			;dost�p do pola docelowego
			mov rax, targetPosition	
			call VectorSplicer
	
			cmp r8, 0
			jl death

			cmp r8, 15
			jg death

			cmp r9, 0
			jl death

			cmp r9, 15
			jg death

			mov rax, mapin
			call ArrayAccByte

			cmp bl, 2
			je death
			
			mov rax, targetPosition
			call VectorSplicer
			mov bl, 1
			mov rax, output
			call ArraySetByte
			mov rax, mapin
			call ArrayAccByte
			mov al, bl
			ret

		tail_move:
			mov rax, targetPosition
			call VectorSplicer
			mov bl, 2
			mov rax, output
			call ArraySetByte
			mov rax, 0
			mov al, 0
			ret

	death:
		mov rax, 1
		ret

MapUpdate endp

VectorSplicer proc ;vector z rax do r8 - x, r9 - y
	mov r8, rax
	mov r9, rax
	and r8, 0FFFFh
	shr r9, 32
	ret

	
VectorSplicer endp


ArrayAccByte proc ;rax - wska�nik r8 - x, r9 - y wynik w bl
	
	mov r11, rax
	mov rax, r8
	shl rax, 3
	add rax, r11
	mov rax, [rax]
	add rax, r9
	mov bl, [rax]	

	ret
ArrayAccByte endp

ArraySetByte proc;rax - wska�nik r8 - x, r9 - y, bl - wartosc
	
	
	mov r11, rax
	mov rax, r8
	shl rax, 3
	add rax, r11
	mov rax, [rax]
	add rax, r9
	mov [rax], bl
	ret
ArraySetByte endp

ArrayAccQword proc;rax - wska�nik r8 - x, r9 - y, rbx - warto��
	
	shl r8, 3 ;mno�enie przez 8
	shl r9, 3
	add rax, r8
	mov rax, [rax]
	add rax, r9
	mov rbx, [rax]
	ret

ArrayAccQword endp


















Paint_Tile  proc X:QWORD, Y:QWORD, Z:QWORD, W:DWORD ;

    LOCAL horizontal:QWORD ;X

    LOCAL vertical:QWORD ;Y

    LOCAL hBmp:QWORD ;Z

    LOCAL hDC:QWORD ;W    
    
    LOCAL hOld:QWORD
    
    LOCAL memDC:QWORD

    mov horizontal, rcx
    mov vertical, rdx

    mov hBmp, r8 
    mov hDC, r9 

    mov rcx,hDC
	;push hDC
    call CreateCompatibleDC
    mov memDC, rax
	mov rdx, memDC
    
	;mov rcx, memDC
    ;mov rdx, hBmp
    push hBmp
	push memDC
	call SelectObject
    mov hOld, rax


	push 00cc0020h
	push 0
    push 0
    push memDC
    push 16
    mov rcx, hDC
    mov rdx, horizontal
    mov r8, vertical
    mov r9, 16
    call BitBlt

    ;mov rcx, hDC
    ;mov rdx, hOld

	push hOld
	push hDC
    call SelectObject

    ;invoke SelectObject,[W],hOld

    mov rcx, memDC
    call DeleteDC




    ret

Paint_Tile endp


Check_Key proc X:QWORD ;Id of the key that will be checked.

	push rcx
    call GetAsyncKeyState ; Correct var allready in rcx
    ret

Check_Key endp



end
