#include "Board.h"
#include <cstdlib>
#include <time.h>
#include "Render.h"
void Board::PostUpdate(bool appled){
    bool inputApplied = false;
    bool appleApplied = false;
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            if (board[i][j] == 1) {
                directions[i][j] = input;
                inputApplied = true;
                if (!appled || appleApplied)
                    return;
            }

            if (appled && board[i][j] == 0 && oldBoard[i][j] == 2) {
                board[i][j] = 2;

                int x;
                int y;
                do {
                    x = rand() % 16;
                    y = rand() % 16;

                } while (board[x][y] != 0);
                board[x][y] = 3;

                appleApplied = true;
                if (inputApplied)
                    return;
            }
        }
    }
}

int Board::GetScore() {
    return length;
}

bool Board::UpdateMap(){
    char** temp = oldBoard;
    oldBoard = board;
    board = temp;

    bool appled = false;

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            board[i][j] = 0;
        }
    }

    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            char res = updF(oldBoard, directions, Vector2Int{ i,j }, board);
            if (res == 1)
                return false;
            else if (res == 3) {
                appled = true;
                length++;
            }
           
        }
    }
   
    PostUpdate(appled);

    return true;


}

char** Board::GetBoard()
{
    return board;
}

Board::Board() :
    board(new char* [16]),
    oldBoard(new char* [16]),
    directions(new Vector2Int* [16]),
    length(2),
    dllHandle(LoadLibrary(L"ASM.dll")),
    updF((AssemblyUpdate)GetProcAddress(dllHandle, "MapUpdate")),
    input(Vector2Int{ 0, -1  })
{
    for (int i = 0; i < 16; i++) {
        board[i] = new char[16];
        oldBoard[i] = new char[16];
        directions[i] = new Vector2Int[16];
        for (int j = 0; j < 16; j++) {
            board[i][j] = 0;
            oldBoard[i][j] = 0;
            directions[i][j] = Vector2Int{ 0, -1 };
        }
    }
    board[7][6] = oldBoard[7][6] = 1;
    board[7][7] = oldBoard[7][7] = 2;
    board[1][1] = oldBoard[1][1] = 3;

    srand(time(NULL));

}

Board::~Board(){
    for (int i = 0; i < 16; i++) {
        delete[] board[i];
        delete[] oldBoard[i];
        delete[] directions[i];
    }
    delete[] board;
    delete[] oldBoard;
    delete[] directions;
}

void Board::SetInput(Vector2Int v2) {
    if (v2.x == 0 && v2.y == 0)
        return;

    input = v2;
}