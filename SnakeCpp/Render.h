#pragma once
#include "header.h"
#include "Board.h"

class Render
{

	HINSTANCE dllHandle;
	Paint_Tile PaintTile;
	Check_Key CheckKey;

	void LoadTextures();

public:
	static Render render;
	Board board;
	HBITMAP tile;
	HBITMAP head;
	HBITMAP snake;
	HBITMAP apple;
	void PaintMapASM(char** _currentMap, HDC hDC);
	void PaintMapCpp(char** _currentMap, HDC hDC);

	bool CheckForExit();
	Vector2Int GetDirection();


	Render();
	~Render();
};

