#pragma once
#include "framework.h"
#include <iostream>
/*
    0 - brak
    1 - g�owa
    2 - ogon
    3 - jab�ko

*/

struct Vector2Int {
    int x;
    int y;
};

const int mapSize = 16;

//                                      mapInput, directions, position,   output (preallocated)
typedef char (_fastcall *AssemblyUpdate)(char**, Vector2Int**, Vector2Int, char**);



typedef void(_fastcall *Paint_Tile)(int, int, HBITMAP, HDC); // draws specified texture, hor and ver must be multiplied by 16 

typedef short(_fastcall* Check_Key)(int); // Checks state of specified key

