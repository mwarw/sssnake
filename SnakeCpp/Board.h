#pragma once
#include "header.h"

#include "framework.h"
class Board{
	char** board;
	char** oldBoard;
	void PostUpdate(bool appled);
	int length;
	Vector2Int** directions;
	HINSTANCE dllHandle;
	AssemblyUpdate updF;
	Vector2Int input;
public:
	int GetScore();
	void SetInput(Vector2Int);
	bool UpdateMap();
	char** GetBoard();
	Board();
	~Board();
};

