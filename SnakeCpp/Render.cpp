#include "Render.h"
#include <iostream>
#include <cstring>
#include <string>

Render Render::render = Render();

void Render::PaintMapASM(char** _currentMap, HDC hDC)
{
	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			PaintTile(i * 16, j * 16, tile, hDC);
		}

	}

}

void Render::PaintMapCpp(char** _currentMap, HDC hDC)
{
	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			HDC hdcMem = CreateCompatibleDC(hDC);

			switch (_currentMap[i][j])
			{
			case 0:
				SelectObject(hdcMem, tile);
				break;
			case 1:
				SelectObject(hdcMem, head);
				break;
			case 2:
				SelectObject(hdcMem, snake);
				break;
			case 3:
				SelectObject(hdcMem, apple);
				break;
			default:
				SelectObject(hdcMem, tile);
				break;
			}




			BitBlt(hDC,32+i*16,32+j*16, 16, 16, hdcMem, 0, 0, SRCCOPY);

			DeleteDC(hdcMem);
		}
	}
}

void Render::LoadTextures()
{

	tile = (HBITMAP)LoadImage(0, L"C://snakeJATextures/emptyTile.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	head = (HBITMAP)LoadImage(0, L"C://snakeJATextures/headTile.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	snake = (HBITMAP)LoadImage(0, L"C://snakeJATextures/snakeTile.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	apple = (HBITMAP)LoadImage(0, L"C://snakeJATextures/appleTile.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
}

bool Render::CheckForExit()
{
	if (CheckKey(0x1B) != 0)
	{
		return true;
	}
	return false;
}

Vector2Int Render::GetDirection()//teoretycznie dziala
{
	int h = 0, v = 0;
	int input = 0;
	//right
	if (CheckKey(0x27) != 0)
	{
		v = 1;
		input++;
	}
	//down
	if (CheckKey(0x28) != 0)
	{
		h = 1;
		input++;
	}
	//up
	if (CheckKey(0x26) != 0)
	{
		h = -1;
		input++;
	}
	//left
	if (CheckKey(0x25) != 0)
	{
		v = -1;
		input++;
	}
	if (input == 1)
	{
		return Vector2Int{v,h};
	}
	return Vector2Int{ 0,0 };
}

Render::Render(): dllHandle(LoadLibrary(L"ASM.dll"))
{
	PaintTile=(Paint_Tile)GetProcAddress(dllHandle, "Paint_Tile");
	CheckKey=(Check_Key)GetProcAddress(dllHandle, "Check_Key");
	
	LoadTextures();
}

Render::~Render()
{
}
