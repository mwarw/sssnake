// SnakeCpp.cpp : Defines the entry point for the application.
//

/*
    0 - brak
    1 - g�owa
    2 - ogon
    3 - w��

*/

#include "Render.h"
#include "framework.h"
#include "SnakeCpp.h"
#include "header.h"
#include "Board.h"
#include <iostream>
#include <string>

#define MAX_LOADSTRING 100


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void DisableCloseButton(HWND hwnd)
{
    EnableMenuItem(GetSystemMenu(hwnd, FALSE), SC_CLOSE,
        MF_DISABLED);
}

void DisableMinimizeButton(HWND hwnd)
{
    SetWindowLong(hwnd, GWL_STYLE,
        GetWindowLong(hwnd, GWL_STYLE) & ~WS_MINIMIZEBOX);
}

void DisableMaximizeButton(HWND hwnd)
{
    SetWindowLong(hwnd, GWL_STYLE,
        GetWindowLong(hwnd, GWL_STYLE) & ~WS_MAXIMIZEBOX);
}

void ExitPopUp(HINSTANCE hInstance)
{
    DWORD dwExtStyle = 0;
    DWORD dwStyle = WS_POPUPWINDOW;

    HWND hWnd = CreateWindowEx(
        dwExtStyle,
        L"Message",
        L"Score",
        dwStyle,
        430,
        430,
        85,
        25,
        NULL,
        NULL,
        hInstance,
        NULL);

    ShowWindow(hWnd, SW_SHOW);
    HDC hDC = GetDC(hWnd);
    TextOut(hDC, 5, 5, L"YOU DIED!", 10);
}

void StartPopUp(HINSTANCE hInstance)
{
    DWORD dwExtStyle = 0;
    DWORD dwStyle = WS_POPUPWINDOW;

    HWND hWnd = CreateWindowEx(
        dwExtStyle,
        L"Message",
        L"Info",
        dwStyle,
        430,
        430,
        130,
        120,
        NULL,
        NULL,
        hInstance,
        NULL);

    ShowWindow(hWnd, SW_SHOW);
    HDC hDC = GetDC(hWnd);
    TextOut(hDC, 12, 15, L"This is Snake!", 15);
    TextOut(hDC, 14,30, L"To move use:", 13);
    TextOut(hDC, 20,45, L"Arrow Keys", 11);
    TextOut(hDC, 7,60, L"It was created by:", 19);
    TextOut(hDC, 14,75, L"-Piotr Bartosz", 15);
    TextOut(hDC, 14,90, L"-Marcin Wola", 13);
}

void CountDownPopUp(HINSTANCE hInstance)
{
    DWORD dwExtStyle = 0;
    DWORD dwStyle = WS_POPUPWINDOW;

    HWND hWnd = CreateWindowEx(
        dwExtStyle,
        L"Message",
        L"CountDown",
        dwStyle,
        430,
        550,
        130,
        50,
        NULL,
        NULL,
        hInstance,
        NULL);

    ShowWindow(hWnd, SW_SHOW);
    HDC hDC = GetDC(hWnd);
    TextOut(hDC, 5, 5, L"Game will start in:", 20);
    TextOut(hDC, 60, 25, L"9", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"8", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"7", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"6", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"5", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"4", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"3", 2);
    Sleep(1000); 
    TextOut(hDC, 60, 25, L"2", 2);
    Sleep(1000); 
    TextOut(hDC, 60, 25, L"1", 2);
    Sleep(1000);
    TextOut(hDC, 60, 25, L"0", 2);
    Sleep(1000);
}


void Die(HWND winHandle) {
    while (true) {
        //�mier�
        Sleep(5000);
        DestroyWindow(winHandle);
        std::exit(0);

    }
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
	

    // TODO: Place code here.
    StartPopUp(hInst);
    CountDownPopUp(hInst);
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_SNAKECPP, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SNAKECPP));

    MSG msg;
    HWND winHandle = GetActiveWindow();
    bool alive = true;
    int gameSpeed = 200; //lower = harder

    DisableCloseButton(winHandle);
    DisableMinimizeButton(winHandle);
    DisableMaximizeButton(winHandle);

    while (alive)//gameloop
    {
        Vector2Int v2 = Render::render.GetDirection();
        Render::render.board.SetInput(v2);
        if(alive && !Render::render.board.UpdateMap()) {
            alive = false;
            ExitPopUp(hInst);
            Die(winHandle);
        }

        InvalidateRect(winHandle, NULL, TRUE);//update mapy
        UpdateWindow(winHandle);

        if (Render::render.CheckForExit())
        {
            DestroyWindow(winHandle);
            return 0;
        }
       Sleep(gameSpeed);


    }
    
    // Main message loop:
   /* while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        if (msg.message != WM_PAINT)
        {
            InvalidateRect(msg.hwnd, NULL, TRUE);
            UpdateWindow(msg.hwnd);
        }
    }
    */
    return 0;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SNAKECPP));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      300, 300 , 360, 360, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {

            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            
            Render::render.PaintMapCpp(Render::render.board.GetBoard(), hdc);
            TextOut(hdc, 210, 295, L"SCORE:", 6);

            int a = 16;
            wchar_t buffer[256];

            TextOut(hdc, 265, 295, buffer, wsprintfW(buffer, L"%d", Render::render.board.GetScore() - 2));//wy�wietlanie wyniku


            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

